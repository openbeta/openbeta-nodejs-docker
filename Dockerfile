FROM debian:bullseye AS tippecanoe-builder

ARG SRC_DIR=/tmp/tippecanoe-src
ARG TC_VERSION=2.41.3

RUN apt-get -qq update \
  && apt-get -qq -y install build-essential libsqlite3-dev zlib1g-dev git \
  && git clone --depth 1 --branch ${TC_VERSION} \
        https://github.com/felt/tippecanoe.git $SRC_DIR

WORKDIR /tmp/tippecanoe-src

RUN make

# Main image
FROM node:18-bullseye-slim

RUN apt-get -qq update \ 
    && apt-get -qq install -y git wget curl unzip gzip tar \
       libsqlite3-dev zlib1g-dev \
    && curl https://rclone.org/install.sh | bash

COPY --from=tippecanoe-builder /tmp/tippecanoe-src/tippecanoe* /usr/local/bin/
